<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mt');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3(/7/Lnwj<?lB1(w`a936q=&/Bgq;mv;lr#*|r5$x]St-HQ#Dg`UhOqjbS ^3O_?');
define('SECURE_AUTH_KEY',  'p]0*4m-@Z@`0V(x*@mzp37tG,T5y)sY25t6J}pl^#:~-/m(6y~4x/$,L!/lhU[5r');
define('LOGGED_IN_KEY',    'WY%b1@JBOgyt+jz^#s6w6^lRkTKUuLbR;-KxlF$0[9|[`)j#Bd G-)*s9TZPOj)}');
define('NONCE_KEY',        'D&~7FIjFGWU]O&@u_A/Nn{>:j2I!p)nA9#3Rt4]#S.uvRmk`(e=l}<~JtfVXN~TG');
define('AUTH_SALT',        'M],4/.:dd3v~$MGGg_N>vJZ?{Df+/p>!MQ7k7OM)u{k&?H^&.v6+v^axPY{<k91t');
define('SECURE_AUTH_SALT', '%!g)CElR@.]@xO|^Q_]!;<6rpHfk3>}hAA6{b[!%.;G@A2Pmjj3?&B>.G8B!E^D]');
define('LOGGED_IN_SALT',   'NP5HOiqr:|DCeGslKGdJjTFiV|kKY~(t3%yZ7B-|/y,B1j3Y_!^8a?z?/;+mMh)K');
define('NONCE_SALT',       'KX_38s!YEv@Tgd_Qhy: pa}:TZk&bA@8^X2JgGoYCt17Km|Y~te%ZU1&)I*F^1oL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
